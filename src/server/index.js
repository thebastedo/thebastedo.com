import http from 'http'
import app from './server'

const port = (process.env.PORT) ? process.env.PORT : 8080

const server = http.createServer(app)

let currentApp = app

server.listen(port, () => {
  console.log('Server Env:', process.env.NODE_ENV)
  console.log('Server Listening on port %s', port)
})

if (module.hot) {
  module.hot.accept('./server', () => {
    server.removeListener('request', currentApp)
    server.on('request', app)
    currentApp = app
  })
}
