const webpack = require('webpack')
const path = require('path')
const nodeExternals = require('webpack-node-externals')
const StartServerPlugin = require('start-server-webpack-plugin')

console.log('Server NODE_ENV: %s', process.env.NODE_ENV)

const production = (process.env.NODE_ENV === 'production')
const isDev = (x) => !production ? x : null
const isProd = (x) => production ? x : null // eslint-disable-line no-unused-vars
const noNulls = (i) => (i)

module.exports = {
  output: {
    path: path.join(__dirname, '.dist'),
    filename: 'server.js',
  },
  entry: [
    isDev('webpack/hot/poll?1000'),
    './src/server/index',
  ].filter(noNulls),
  watch: !production,
  target: 'node',
  externals: [nodeExternals({
    whitelist: [isDev('webpack/hot/poll?1000')].filter(noNulls),
  })],
  resolve: {
    modules: [
      path.resolve('./src'),
      path.resolve('./node_modules'),
    ],
  },
  module: {
    rules: [
      { test: /\.js$/, use: 'babel-loader', exclude: /node_modules/ },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'css-loader',
            options: {
              sourceMap: false,
              importLoaders: 2,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: false,
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: false,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new StartServerPlugin('server.js'),
    isDev(new webpack.NamedModulesPlugin()),
    isDev(new webpack.HotModuleReplacementPlugin()),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': (production) ? JSON.stringify('production') : JSON.stringify('development'),
        'BUILD_TARGET': JSON.stringify('server'),
        'PORT': (process.env && process.env.PORT) ? process.env.PORT : false,
      },
    }),
  ].filter(noNulls),
}
