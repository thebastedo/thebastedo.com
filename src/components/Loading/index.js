import React from 'react'
import Wrapper from './Wrapper'
import Spinner from './Spinner'

export default function Loading () {
  return (
    <Wrapper>
      <Spinner/>
    </Wrapper>
  )
}
