import React, { Component } from 'react'

export default class Footer extends Component {
  render () {
    return (
      <footer className='footer'>
        <div className='container'>
          <div className='content has-text-centered'>
            &copy; 2017 thebastedo.com
          </div>
        </div>
      </footer>
    )
  }
}
