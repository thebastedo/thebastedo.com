import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import MarkdownRenderer from 'react-markdown-renderer'
import moment from 'moment'
import Loading from 'components/Loading'

class BlogEntry extends Component {
  render () {
    if (this.props.isLoading || !this.props.data || !this.props.data.Blog) {
      return <Loading />
    }

    let blog = this.props.data.Blog
    let tags = (Array.isArray(blog.tags)) ? ` on ${blog.tags.join(', ')}` : null
    let subtitle = (blog.subtitle) ? <div className='subtitle'>{blog.subtitle}</div> : null

    return (
      <div>
        <Helmet>
          <title>{blog.title}</title>
        </Helmet>
        <div className='title'>{blog.title}</div>
        {subtitle}
        <div><small>{moment(blog.createdAt).format('LLL')}{tags}</small></div>
        <hr />
        <div className='content'><MarkdownRenderer markdown={blog.body} /></div>
      </div>
    )
  }
}

const blogEntry = gql`
  query blogEntries($slug: String!) {
    Blog(slug: $slug) {
      id
      createdAt
      title
      subtitle
      body
      tags
    }
}`

export default graphql(blogEntry, {
  options: (props) => {
    return { variables: { slug: props.match.params.blogSlug } }
  },
})(BlogEntry)
