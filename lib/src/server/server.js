'use strict';

var _http = require('http');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _server = require('react-dom/server');

var _server2 = _interopRequireDefault(_server);

var _reactRouter = require('react-router');

var _App = require('../App');

var _App2 = _interopRequireDefault(_App);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

require('babel-register');

(0, _http.createServer)(function (req, res) {
  var context = {};

  var html = _server2.default.renderToString(_react2.default.createElement(
    _reactRouter.StaticRouter,
    {
      location: req.url,
      context: context
    },
    _react2.default.createElement(_App2.default, null)
  ));

  if (context.url) {
    res.writeHead(301, {
      Location: context.url
    });
    res.end();
  } else {
    res.write('\n      <!doctype html>\n      <div id="app">' + html + '</div>\n    ');
    res.end();
  }
}).listen(3000);