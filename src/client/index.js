import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import { ApolloProvider } from 'react-apollo'
import client from '../common/transport'
import Router from './router'

const render = (Component) => {
  ReactDOM.hydrate(
    <AppContainer>
      <ApolloProvider client={client}>
        <Component/>
      </ApolloProvider>
    </AppContainer>,
    document.getElementById('content')
  )
}

window.onload = () => {
  render(Router)

  // Hot Module Replacement API
  if (module.hot) {
    module.hot.accept('./router', () => {
      render(Router)
    })
  }
}
