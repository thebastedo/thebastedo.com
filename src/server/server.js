import 'isomorphic-fetch'

import express from 'express'
import morgan from 'morgan'
import compression from 'compression'
import helmet from 'helmet'
import path from 'path'

import React from 'react'
import { StaticRouter } from 'react-router'
import { ApolloProvider, renderToStringWithData } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ServerStyleSheet } from 'styled-components'
import Helmet from 'react-helmet'

import App from '../App'

const app = express()

// Logging
const logFormat = (process.env.NODE_ENV === 'production') ? 'common' : 'dev'
app.use(morgan(logFormat))

// GZip Compression
app.use(compression())

// Security
app.use(helmet())

// Templating Engine
app.set('view engine', 'ejs')
app.set('views', path.resolve('./src/server/templates'))

// Static routes
app.use(express.static(path.resolve('./public')))

// Testing Routes
app.use('/wonkles', (req, res) => {
  res.send('I am working!')
})

app.use('/flonkles', (req, res) => {
  res.send('Wonkles!')
})

app.use((req, res) => {
  const client = new ApolloClient({
    ssrMode: true,
    // Remember that this is the interface the SSR server will use to connect to the
    // API server, so we need to ensure it isn't firewalled, etc
    link: createHttpLink({
      uri: 'https://api.graphcms.com/simple/v1/cj1eh7f9k5izd0198mczqmjph',
      credentials: 'same-origin',
      headers: {
        cookie: req.header('Cookie'),
      },
    }),
    cache: new InMemoryCache(),
  })

  const sheet = new ServerStyleSheet()
  const context = {}

  const page = (
    <ApolloProvider client={client}>
      <StaticRouter location={req.url} context={context}>
        <App/>
      </StaticRouter>
    </ApolloProvider>
  )

  renderToStringWithData(sheet.collectStyles(page)).then((content) => {
    if (context.url) {
      res.writeHead(301, {
        Location: context.url,
      })
      res.end()
    } else {
      const production = process.env.NODE_ENV === 'production'
      const initialState = client.extract()
      const state = JSON.stringify(initialState).replace(/</g, '\\u003c')
      const helmet = Helmet.renderStatic()
      const head = {
        title: helmet.title.toString(),
        meta: helmet.meta.toString(),
        link: helmet.link.toString(),
        style: sheet.getStyleTags(),
      }

      res.status(200).render('index', {
        html: content,
        state,
        head,
        production,
      })
    }
  }).catch((error) => {
    console.log('error rendering page...')
    console.log(error)
    res.status(500).send('The server encountered an error.')
  })
})

export default app
