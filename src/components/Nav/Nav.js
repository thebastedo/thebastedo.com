import React, { Component } from 'react'
import { Link, NavLink } from 'react-router-dom'
import classNames from 'classnames'

export default class Nav extends Component {
  constructor (props) {
    super(props)

    this.state = {
      navMenuActive: false,
    }
  }

  toggleNav = () => {
    this.setState({ navMenuActive: !this.state.navMenuActive })
  }

  menuClick = () => {
    if (this.state.navMenuActive) {
      this.toggleNav()
    }
  }

  render () {
    return (
      <nav className='navbar has-shadow'>
        <div className='navbar-brand'>
          <div className='navbar-item'>
            <Link to='/'>thebastedo.com</Link>
          </div>
          <div className={classNames('navbar-burger', 'burger', { 'is-active': this.state.navMenuActive })} onClick={this.toggleNav}>
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
        <div className={classNames('navbar-menu', { 'is-active': this.state.navMenuActive })} onClick={this.menuClick}>
          <div className='navbar-end'>
            <NavLink className='navbar-item is-tab' activeClassName='is-active' to='/' exact>Home</NavLink>
            <NavLink className='navbar-item is-tab' activeClassName='is-active' to='/blog'>Blog</NavLink>
            <NavLink className='navbar-item is-tab' activeClassName='is-active' to='/projects' exact>Projects</NavLink>
            <div className='navbar-item'>
              <div className='field is-grouped'>
                <p className='control'>
                  <a className='button is-white' href='https://github.com/thebastedo'><span className='icon'><i className='fa fa-github'></i></span></a>
                </p>
                <p className='control'>
                  <a className='button is-white' href='https://www.linkedin.com/in/thebastedo'><span className='icon'><i className='fa fa-linkedin'></i></span></a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </nav>
    )
  }
}
