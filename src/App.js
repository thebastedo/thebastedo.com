import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import Layout from './components/Layout/Layout'
import Home from './views/Home'
import Blog from './views/Blog/Blog'
import NoMatch from './views/NoMatch'

import './Bulma.scss'

export default class App extends Component {
  render () {
    return (
      <Layout>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/blog' component={Blog}/>
          <Route component={NoMatch}/>
        </Switch>
      </Layout>
    )
  }
}
