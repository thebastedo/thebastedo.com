import React, { Component } from 'react'
import { gql, graphql } from 'react-apollo'

class BlogList extends Component {
  renderEntries () {
    if (this.props.isLoading || !this.props.data || !this.props.data.allBlogs) {
      return <div>Loading...</div>
    }

    return (
      this.props.data.allBlogs.map((blog) => {
        return (
          <div key={blog.id}>{blog.title}</div>
        )
      })
    )
  }

  render () {
    return (
      <div>
        <div>Blog List</div>
        {this.renderEntries()}
      </div>
    )
  }
}

const blogEntries = gql`
  query blogEntries {
    allBlogs(orderBy:createdAt_ASC) {
      id
      title
      createdAt
      body
    }
  }`

export default graphql(blogEntries)(BlogList)
