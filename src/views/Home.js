import React, { Component } from 'react'

export default class Home extends Component {
  render () {
    return (
      <section>
        <div className='hero is-primary is-bold'>
          <div className='hero-body'>
            <h1 className='title'>Home</h1>
            <h2 className='subtitle'>Welcome to my crazy webpage.</h2>
          </div>
        </div>
        <article className='section'>
          <article className='media'>
            <div className='media-content'>
              <div className='content'>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent dapibus condimentum lorem non tincidunt. Suspendisse blandit, eros non placerat faucibus, nibh tortor semper felis, vel viverra dolor ligula et est. Mauris commodo lectus congue nisl dignissim tristique. Quisque quam risus, rutrum imperdiet fringilla eget, fringilla eget mauris. Pellentesque rhoncus vulputate euismod. Nunc fringilla suscipit dui. Phasellus condimentum vehicula lorem, sit amet laoreet lectus luctus eget. In felis velit, aliquam sed quam id, pulvinar semper nibh. Suspendisse eu velit gravida, suscipit erat vel, molestie velit.
                </p>
                <p>
                  Suspendisse accumsan erat ipsum, feugiat condimentum felis ultrices vel. Fusce fermentum purus id mi euismod, mattis consequat ligula efficitur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut sapien erat, dapibus vitae erat vel, consectetur pharetra est. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse in semper nulla. Aliquam pharetra quam ipsum, a tempus velit venenatis ac. Curabitur tempor felis nisl. Nullam ac placerat enim. Integer in pellentesque tortor. Phasellus vel odio arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed pellentesque justo id arcu eleifend, ut finibus purus dapibus. In vel nunc ut risus pulvinar pretium at sit amet erat. Etiam luctus sed ligula non tincidunt. Donec tempor efficitur elit, malesuada semper nibh facilisis eget.
                </p>
              </div>
            </div>
          </article>
        </article>
      </section>
    )
  }
}
