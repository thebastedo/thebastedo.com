import React, { Component } from 'react'
import { Helmet } from 'react-helmet'

import Nav from '../Nav/Nav'
import Footer from '../Footer/Footer'

export default class Layout extends Component {
  render () {
    return (
      <div>
        <Helmet titleTemplate='thebastedo.com - %s' defaultTitle='thebastedo.com'/>
        <Nav/>
        <div>{this.props.children}</div>
        <Footer/>
      </div>
    )
  }
}
