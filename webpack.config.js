const webpack = require('webpack')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const path = require('path')

console.log('Client NODE_ENV: %s', process.env.NODE_ENV)

const production = (process.env.NODE_ENV === 'production')
const isDev = (x) => !production ? x : null
const isProd = (x) => production ? x : null
const noNulls = (i) => (i)

const buildPath = path.resolve('./public')
const sourcePath = path.resolve('./src')

let entry = [
  'babel-polyfill',
  isDev('react-hot-loader/patch'),
  isDev('webpack-dev-server/client?http://localhost:8080'),
  isDev('webpack/hot/only-dev-server'),
  './client/index',
].filter(noNulls)

let plugins = [
  new webpack.NamedModulesPlugin(),
  isDev(new webpack.HotModuleReplacementPlugin()),
  isDev(new webpack.NoEmitOnErrorsPlugin()),
  isProd(new UglifyJSPlugin()),
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': (production) ? JSON.stringify('production') : JSON.stringify('development'),
      'BUILD_TARGET': JSON.stringify('client'),
    },
  }),
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    minChunks (module) {
      var context = module.context
      return context && context.indexOf('node_modules') >= 0
    },
  }),
  new ExtractTextPlugin({
    filename: 'styles.css',
    disable: !production,
  }),
].filter(noNulls)

const sourceMap = !production

module.exports = {
  devtool: (production) ? false : 'inline-source-map',
  context: sourcePath,
  entry: entry,
  output: {
    path: buildPath,
    filename: '[name].js',
  },
  resolve: {
    extensions: ['.js'],
    modules: [
      path.resolve('./src'),
      path.resolve('./node_modules'),
    ],
  },
  plugins: plugins,
  module: {
    rules: [
      {
        test: /\.js$/,
        include: sourcePath,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap,
                importLoaders: 2,
                minimize: production,
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                sourceMap,
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap,
              },
            },
          ],
        }),
      },
    ],
  },
  devServer: {
    host: 'localhost',
    port: 8080,
    historyApiFallback: true,
    hot: true,
    proxy: {
      '/': { target: 'http://localhost:8081' },
    },
  },
}
