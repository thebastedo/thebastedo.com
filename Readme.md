[![build status](https://gitlab.com/thebastedo/thebastedo.com/badges/master/build.svg)](https://gitlab.com/thebastedo/thebastedo.com/commits/master) [![coverage report](https://gitlab.com/thebastedo/thebastedo.com/badges/master/coverage.svg)](https://gitlab.com/thebastedo/thebastedo.com/commits/master) [![style: styled-components](https://img.shields.io/badge/style-%F0%9F%92%85%20styled--components-orange.svg?colorB=daa357&colorA=db748e)](https://github.com/styled-components/styled-components)

# [thebastedo.com]

Source code for the personal website [thebastedo.com]

## Tech Stack

graphcms
react
webpack
jest
bulma.io
express

## CI/CD

## Testing

## Server Rendering
