import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
import { Link, Route } from 'react-router-dom'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import moment from 'moment'
import Loading from 'components/Loading'

import BlogEntry from './BlogEntry'

class Blog extends Component {
  renderEntries () {
    if (this.props.isLoading || !this.props.data || !this.props.data.allBlogs) {
      return <Loading />
    } else if (this.props.data && this.props.data.allBlogs && this.props.data.allBlogs.length === 0) {
      return <div>No Blogs</div>
    }

    return (
      this.props.data.allBlogs.map((blog) => {
        let tags = blog.tags || []
        tags = tags.join(', ')

        return (
          <article className='container' key={blog.id} style={{cursor: 'pointer'}}>
            <Link to={`${this.props.match.url}/${blog.slug}`}>
              <div className='content'>
                <div className='content-body'>
                  <h1 className='title is-3'>{blog.title}</h1>
                  <h2 className='subtitle is-6'>{tags}</h2>
                  <small>Posted {moment(blog.createdAt).fromNow()}</small>
                </div>
              </div>
            </Link>
            <hr />
          </article>
        )
      })
    )
  }

  render () {
    return (
      <div>
        <Helmet>
          <title>Blog</title>
        </Helmet>
        <section>
          <div className='hero is-info is-bold'>
            <div className='hero-body'>
              <h1 className='title'>Blog</h1>
              <h2 className='subtitle'>My madness published online</h2>
            </div>
          </div>
          <div className='section'>
            <Route path={`${this.props.match.url}/:blogSlug`} component={BlogEntry}/>
            <Route exact path={this.props.match.url} render={() => (
              <div>{this.renderEntries()}</div>
            )}/>
          </div>
        </section>
      </div>
    )
  }
}

const blogEntries = gql`
  query blogEntries {
    allBlogs(orderBy:createdAt_ASC, filter: { isPublished: true }) {
      id
      title
      slug
      createdAt
      tags
    }
  }`

export default graphql(blogEntries)(Blog)
